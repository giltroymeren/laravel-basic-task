<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Task;
use Illuminate\Http\Request;

Route::bind('tasks', function($value, $route) {
    return App\Task::whereSlug($value)->first();
});

/**
 * Show Task Dashboard
 */
Route::get('/{url}', function () {
    $tasks = Task::orderBy('created_at', 'asc')->get();

    return view('tasks', [
        'tasks' => $tasks,
        'action' => 'Create',
        'form_action' => '/tasks/create'
    ]);
})->where('url', '(tasks)?');

/**
 * Add New Task
 */
Route::post('/tasks/create', function (Request $request) {
    $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
    ]);

    if ($validator->fails()) {
        return redirect('/')
            ->withInput()
            ->withErrors($validator);
    }

    $task = new Task;
    $task->name = $request->name;
    $task->save();

    return redirect('/');
});

/**
 * Update A New Task
 * Show Update Form
 */
Route::get('/tasks/update/{id}', function ($id) {
    $tasks = Task::orderBy('created_at', 'asc')->get();
    $task = Task::where('id', $id)->get();

    return view('tasks', [
        'tasks' => $tasks,
        'task' => $task,
        'action' => 'Update',
        'form_action' => '/tasks/update'
    ]);
});

/**
 * Update A New Task
 * Save Updates
 */
Route::post('/tasks/update', function (Request $request) {
    $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
    ]);

    if ($validator->fails()) {
        return redirect('/tasks/update/'.$request->id)
            ->withInput()
            ->withErrors($validator);
    }

    $task = Task::find($request->id);
    $task->name = $request->name;
    $task->save();

    return redirect('tasks');
});

/**
 * Delete Task
 */
Route::delete('/tasks/delete/{task}', function (Task $task) {
    $task->delete();

    return redirect('/');
});